import math
from tkinter import *
from turtle import width
from typing import List

root = Tk()
root.title("Calculator")
e = Entry(root, width=35, borderwidth=5)

def main():
    buttonsPlaced()
    e.grid(row=0, column=0, columnspan=3, padx=10, pady=10)
    root.mainloop()

def buttonsPlaced():
    #Clear
    ButtonClear = buttonClear("C")
    buttonOnScreenClear(ButtonClear)
    #Divide
    ButtonDivide = buttonDivide("/")
    buttonOnScreen(ButtonDivide, 4, 2)
    #Subtract
    ButtonSubtract = buttonSubtract("-")
    buttonOnScreen(ButtonSubtract, 5, 1)
    #Multiply
    ButtonMultiply = buttonMultiply("*")
    buttonOnScreen(ButtonMultiply, 5, 0)
    #Equals
    ButtonEquals = buttonEqual("=")
    buttonOnScreen(ButtonEquals, 5, 2)
    #Addition
    ButtonAddition = buttonAdd("+")
    buttonOnScreen(ButtonAddition, 4, 1)
    #Button 0
    Button0 = button("0", 0)
    buttonOnScreen(Button0, 4, 0)
    #Button 1
    Button1 = button("1", 1)
    buttonOnScreen(Button1, 3, 0)
    #Button 2
    Button2 = button("2", 2)
    buttonOnScreen(Button2, 3, 1)
    #Button 3
    Button3 = button("3", 3)
    buttonOnScreen(Button3, 3, 2)
    #Button 4
    Button4 = button("4", 4)
    buttonOnScreen(Button4, 2, 0)
    #Button 5
    Button5 = button("5", 5)
    buttonOnScreen(Button5, 2, 1)
    #Button 6
    Button6 = button("6", 6)
    buttonOnScreen(Button6, 2, 2)
    #Button 7
    Button7 = button("7", 7)
    buttonOnScreen(Button7, 1, 0)
    #Button 8
    Button8 = button("8", 8)
    buttonOnScreen(Button8, 1, 1)
    #Button 9
    Button9 = button("9", 9)
    buttonOnScreen(Button9, 1, 2)

def button(x, number):
    return Button(root, text=x, padx=40, pady=20, command=lambda: buttonClick(number))

def buttonClear(x):
    return Button(root, text=x, padx=40, pady=20, command=lambda: clear())

def buttonAdd(x):
    return Button(root, text=x, padx=39, pady=20, command=lambda: add())

def buttonSubtract(x):
    return Button(root, text=x, padx=40, pady=20, command=lambda: subtract())

def buttonMultiply(x):
    return Button(root, text=x, padx=40, pady=20, command=lambda: multiply())

def buttonDivide(x):
    return Button(root, text=x, padx=40, pady=20, command=lambda: divide())

def buttonEqual(x):
    return Button(root, text=x, padx=40, pady=20, command=lambda: equals())
    
def buttonOnScreen(Button, x, y):
    return Button.grid(row=x, column=y)

def buttonOnScreenClear(Button):
    return Button.grid(row=6, column=2)

def equals():
    second_number = e.get()
    e.delete(0, END)

    if math == "addition":
        e.insert(0, f_num + int(second_number))

    if math == "subtraction":
        e.insert(0, f_num - int(second_number))

    if math == "multiplication":
        e.insert(0, f_num * int(second_number))

    if math == "division":
        e.insert(0, f_num // int(second_number))

def add():
    first_number = e.get()
    global f_num
    global math
    math = "addition"
    f_num = int(first_number)
    e.delete(0, END)

def subtract():
    first_number = e.get()
    global f_num
    global math
    math = "subtraction"
    f_num = int(first_number)
    e.delete(0, END)

def multiply():
    first_number = e.get()
    global f_num
    global math
    math = "multiplication"
    f_num = int(first_number)
    e.delete(0, END)

def divide():
    first_number = e.get()
    global f_num
    global math
    math = "division"
    f_num = int(first_number)
    e.delete(0, END)

def clear():
    e.delete(0, END)

def buttonClick(number):
    current = e.get()
    e.delete(0, END)
    e.insert(0, str(current) + str(number))

if __name__ == '__main__':
    main()
